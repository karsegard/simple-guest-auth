# This is my package simple-guest-auth

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/simple-guest-auth.svg?style=flat-square)](https://packagist.org/packages/kda/simple-guest-auth)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/simple-guest-auth.svg?style=flat-square)](https://packagist.org/packages/kda/simple-guest-auth)

## Installation

You can install the package via composer:

```bash
composer require kda/simple-guest-auth
```


You have to add the Middleware to your app/Http/Kernel.php

```php
 protected $routeMiddleware = [
    //other middlewares
    'guest_shield' => \KDA\SimpleGuestAuth\Middleware::class,
];
```

Then create a route with the login page. routes/web.php

```php
Route::get('/login',  \KDA\SimpleGuestAuth\Livewire\Login::class);
```
note: If you don't have livewire configured you have to create an app layout for this route to work. https://laravel-livewire.com/docs/2.x/rendering-components#page-components


### optional
You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="KDA\SimpleGuestAuth\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="KDA\SimpleGuestAuth\ServiceProvider" --tag="config"
```



you can publish the views using

```bash
php artisan vendor:publish --provider="KDA\SimpleGuestAuth\ServiceProvider" --tag="views"
```

### protect your routes

You are ready to protect your route with a password.  Note that the route has to have a name.

```php
Route::middleware('guest_shield')->get('/', function () {
    return view('welcome');
})->name('gallery');
```

Create a shield for a route name

```bash
php  artisan kda:guest-shield:create gallery
```
## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
