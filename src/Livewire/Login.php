<?php

namespace KDA\SimpleGuestAuth\Livewire;

use KDA\SimpleGuestAuth\Facades\Shield;
use Livewire\Component;

class Login extends Component
{
    public $password;
    public $error='';
    public function render()
    {
        return view('kda-simple-guest-auth::livewire.login')
        ->layout(config('kda.simple-guest-auth.livewire_layout','layouts.app'));
    }

    public function auth()
    {
        if (Shield::attemptAuth($this->password)) {
            return redirect()->route(Shield::getRoute());
        }else{
            $this->error = 'invalid code';
        }
    }
}
