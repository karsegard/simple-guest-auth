<?php

namespace KDA\SimpleGuestAuth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'key',
        'shield_id',
        'expires_at',
    ];

    protected $appends = [

    ];

    protected $casts = [
        'expires_at' => 'datetime',
    ];

    protected static function newFactory()
    {
        return  \KDA\SimpleGuestAuth\Database\Factories\GuestUserFactory::new();
    }
}
