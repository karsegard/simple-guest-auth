<?php

namespace KDA\SimpleGuestAuth;

use Hash;
use KDA\SimpleGuestAuth\Models\GuestShield;
use KDA\SimpleGuestAuth\Models\GuestUser;
use Session;
use Str;

class Shield
{
    public function getRoute()
    {
        return  Session::get('guest_shield.intended_route');
    }

    public function create()
    {
        return GuestShield::create([
            'route' => $route,
            'password' => $password,
        ]);
    }

    public function attemptAuth($password):bool
    {
        $route = Session::get('guest_shield.intended_route');
        $shield = GuestShield::where('route', $route)->get();
        foreach ($shield as $s) {
            if (Hash::check($password, $s->password)) {
                $session = GuestUser::create([
                    'shield_id' => $s->id,
                    'key' => Str::uuid()->toString(),
                    'expires_at' => \Date::now()->addMinutes(config('kda.simple-guest-auth.session_validity_minutes', 60)),
                ]);
                Session::put('guest_shield.allowed.'.$route, $session->key);
                Session::save();

                return true;
            }
        }

        return false;
    }
}
