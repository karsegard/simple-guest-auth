<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('guest_users', function (Blueprint $table) {
            $table->id();
            $table->string('shield_id'); // authorized for after validating password
            $table->string('key'); // authorized for after validating password
            $table->datetime('expires_at'); // store it as unixtimestamp now()->addHours(5)->timestamp
            $table->timestamps();
        });

        Schema::create('guest_shields', function (Blueprint $table) {
            $table->id();
            $table->string('route'); // authorized for after validating password
            $table->string('password'); // store it as unixtimestamp now()->addHours(5)->timestamp
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('guest_shields');
        Schema::dropIfExists('guest_users');

        Schema::enableForeignKeyConstraints();
    }
};
