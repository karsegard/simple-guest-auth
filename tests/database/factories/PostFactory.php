<?php

namespace KDA\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\Post;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
