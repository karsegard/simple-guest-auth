<?php

namespace KDA\SimpleGuestAuth;

use Closure;
use KDA\SimpleGuestAuth\Models\GuestUser;
use Session;

class Middleware
{
    protected array | null $except;

    public function __construct()
    {
        $this->except = config('kda.kda.simple-guest-auth.exceptions', ['/login']);
    }

    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

    public function handle($request, Closure $next)
    {
        $alloweds = session()->get('guest_shield.allowed');
        $route = \Route::current()->getName();
        if (isset($alloweds[$route])) {
            $session = GuestUser::where('key', $alloweds[$route])->where('expires_at', '>', \Date::now())->first();
            if ($session) {
                return $next($request);
            }
        }
        Session::put('guest_shield.intended_route', $route);
        Session::save();

        return redirect()->to(config('kda.simple-guest-auth.login_url', '/login'));
    }
}
