<div>
    <form wire:submit.prevent="auth">
    The page you requested requires a password <input type="password" wire:model="password"/>

    <button type="submit">Login</button>
    @if(!empty($error))
        {{$error}}
    @endif
    </form>
</div>