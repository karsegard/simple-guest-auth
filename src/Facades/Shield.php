<?php

namespace KDA\SimpleGuestAuth\Facades;

use Illuminate\Support\Facades\Facade;

class Shield extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'kda.guest_shield';
    }
}
