<?php

namespace KDA\SimpleGuestAuth\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\SimpleGuestAuth\Models\GuestUser;

class GuestUserFactory extends Factory
{
    protected $model = GuestUser::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
