<?php

// config for KDA/SimpleGuestAuth
return [
    'login_url' => '/login',
    'session_validity_minutes' => 60,
    'livewire_layout'=>'layouts.app'
];
