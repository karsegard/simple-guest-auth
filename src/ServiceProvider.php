<?php

namespace KDA\SimpleGuestAuth;

use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
use KDA\SimpleGuestAuth\Livewire\Login;
use Livewire\Livewire;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasLoadableMigration;

    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    // trait \KDA\Laravel\Traits\HasConfig;
    //    registers config file as
    //      [file_relative_to_config_dir => namespace]
    protected $_commands = [
        \KDA\SimpleGuestAuth\Commands\CreateShieldCommand::class,
    ];

    protected $configDir = 'config';

    protected $viewNamespace = 'kda-simple-guest-auth';

    protected $publishViewsTo = 'vendor/kda-simple-guest-auth';

    protected $configs = [
        'kda/simple-guest-auth.php' => 'kda.simple-guest-auth',
    ];

    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
        $this->app->bind('kda.guest_shield', Shield::class);
    }

    //called after the trait were booted
    protected function bootSelf()
    {
        Livewire::component(Login::getName(), Login::class);
    }
}
