<?php

namespace KDA\SimpleGuestAuth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuestShield extends Model
{
    use HasFactory;

    protected $fillable = [
        'route',
        'password',
    ];

    protected $appends = [

    ];

    protected $casts = [
        'expires_at' => 'datetime',
    ];

    protected $hidden = [
        'password',
    ];

    protected static function newFactory()
    {
        return  \KDA\SimpleGuestAuth\Database\Factories\GuestUserFactory::new();
    }

    public function setPasswordAttribute($value)
    {
        if (\Hash::needsRehash($value)) {
            //dd('hashed_password');
            $value = \Hash::make($value);
        } else {
            // dd('not hashed',$value);
        }
        $this->attributes['password'] = $value;
    }
}
