<?php

namespace KDA\SimpleGuestAuth\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use KDA\SimpleGuestAuth\Models\GuestShield;

class CreateShieldCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:guest-shield:create {route}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create guest shield';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $route = $this->argument('route');
        $password = $this->secret('Type your password');
        GuestShield::create([
            'route' => $route,
            'password' => $password,
        ]);
    }
}
